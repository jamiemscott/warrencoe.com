$( document ).ready(function() {

	// A bit of form valdation
	$('.ui.form').form({

		firstName: {
			identifier  	: 'first-name',
			rules: [
			{
				type   	: 'empty',
				prompt 	: 'What\'s your first name?'
			}
			]
		},

		lastName: {
			identifier  	: 'last-name',
			rules: [
			{
				type   	: 'empty',
				prompt 	: 'What\'s your last name?'
			}
			]
		},

		email: {
			identifier  	: 'email',
			rules: [
			{
				type   	: 'email',
				prompt 	: 'What\'s your email address?'
			}
			]
		},

		telephone: {
			identifier  	: 'telephone',
			rules: [
			{
				type	: 'empty',
				prompt 	: 'What\'s your telephone number?'
			}
			]
		},

		town: {
			identifier  	: 'town',
			rules: [
			{
				type	: 'empty',
				prompt 	: 'Where are you from?'
			}
			]
		},

		validation: {
			identifier  	: 'validation',
			rules: [
			{
				type	: 'empty',
				prompt	: 'Whoops missed this one'
			},
			{
				type	: 'is[4]', 
				prompt 	: 'Try again',
			}
			]
		},

		message: {
			identifier  	: 'message',
			rules: [
			{
				type	: 'empty',
				prompt 	: 'Please leave me a short message.'
			}
			]
		},

	},

	{
		on: 'blur',
		inline: 'true'
	});

	$('#thank-you .close').on('click', function() {
	  $(this).closest('.message').fadeOut();
	  setTimeout(
	  function() 
	  {
	    window.location.href = window.location.href;
	  }, 1000);
	});

	$('#browser-warning .close').on('click', function() {
	  $(this).closest('.message').fadeOut();
	});

	/*Scroll the page from an anchor to a target with the same name*/
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});

	/*Hmmm cookies*/
	$.cookieCuttr(); 

});