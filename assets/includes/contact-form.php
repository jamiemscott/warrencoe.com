<?php 

    //Main variables
    $domain = 'http://' . $_SERVER['HTTP_HOST']; // Root Domain - http://example.com
    $siteName = 'Warren Coe';
    $siteEmail = 'info@warrencoe.com';
    $error = '';

    // Check if the web form has been submitted
    if (isset($_POST["email"])){

        /*
         * Process the web form variables
         * Strip out any malicious attempts at code injection, just to be safe.
         * All that is stored is safe html entities of code that might be submitted.
         */

        //Email variables
        $subject = $_POST['subject'];
            $firstName = htmlentities(substr($_POST['first-name'], 0 , 100), ENT_QUOTES);   
            $lastName = htmlentities(substr($_POST['last-name'], 0 , 100), ENT_QUOTES);
        $contactName = $firstName . " " . $lastName;
        $email = htmlentities(substr($_POST['email'], 0 , 100), ENT_QUOTES);
        $telephone = htmlentities(substr($_POST['telephone'], 0 , 100), ENT_QUOTES);    
        $town = htmlentities(substr($_POST['town'], 0 , 100), ENT_QUOTES);  
        $validation = htmlentities(substr($_POST['validation'], 0 , 4), ENT_QUOTES);
        $contactChoice = $_POST['contact-choice'];
        $message = htmlentities(substr($_POST['message'], 0, 10000), ENT_QUOTES);

        $emailRegex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
        $phoneRegex = '/^\(?0( *\d\)?){9,10}$/';

        $thankyou = '<div id="thank-you" class="column">
                        <div class="ui success message">
                            <i class="close icon"></i>
                            <div class="header">
                                Your message has been submitted!
                            </div>
                            <p>Thanks for getting in touch, your custom means a lot to me. I\'ll get back to you as soon as possible</p>
                        </div>
                    </div>';

        $failure = '<div id="error" class="column">
                        <div class="ui error message">
                            <i class="close icon"></i>
                            <div class="header">
                                Something has gone wrong!
                            </div>
                            <p>We were not able to send your message. Please contact ' . $siteEmail . '</p>
                        </div>
                    </div>';

        /*
        * Perform some logic on the form data
        * If the form data has been entered incorrectly, return an Error Message
        */

        // Run some error checks
        if ($firstName == '') {
            $error .= '<li>What\'s your first name?</li>';
        }

        if ($lastName == '') {
            $error .= '<li>What\'s your last name?</li>';
        }

        if ($email == '') {
            $error .= '<li>What\'s your email address?</li>';
        }

        if (!preg_match($emailRegex, $email)) {
            $error .= '<li>Please enter a valid e-mail address.</li>';
        }

        if ($telephone == '') {
            $error .= '<li>What\'s your telephone number?</li>';
        }

        if (!preg_match($phoneRegex, $telephone)) {
            $error .= '<li>Please enter a valid telephone number.</li>';
        }

        if ($town == '') {
            $error .= '<li>Where are you from?</li>';
        }

        if ($validation == '') {
            $error .= '<li>Whoops missed this one, what\'s 2+2?</li>';
        } elseif ($validation != '4') {
            $error .= '<li>Try again, what\'s 2+2?</li>';
        }

        if ($message == '') {
            $error .= '<li>Please leave a short message</li>';
        }

        // IF NO ERROR - START
        if ($error == ''){
            
            // Prepare the E-mail elements to be sent
            $emailSubject = $subject;
            $emailMessage = 
            '<html>
                <head>
                <title>' . $siteName . ': A Contact Message</title>
                </head>
                <body>'
                    . $contactName .' from ' . $town . ' contacted you via the website contact form. <br />'
                    . $firstName . ' can be reached by telephone on ' . $telephone . 
                    ' or via email on ' . $email . ' but would prefer to be contacted via ' . $contactChoice . '.<br /><br />'
                    . $firstName . ' left you the following message: <br /><br />'
                    . wordwrap($message, 100) .' 
                </body>
            </html>';
            
            /*
             * We are sending the E-mail using PHP's mail function
             * To make the E-mail appear more legit, we are adding several key headers
             * You can add additional headers later to futher customize the E-mail
             */
            
            $to = $siteName . ' Contact Form <' . $siteEmail . '>';
            
            // To send HTML mail, the Content-type header must be set
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            
            // Additional Headers
            $headers .= 'From: ' . $contactName . ' <' . $email . '>' . "\r\n";
            $headers .= 'Reply-To: ' . $contactName . ' <' . $email . '>' . "\r\n";
            $headers .= 'Return-Path: ' . $contactName . ' <' . $email . '>' . "\r\n";
            $headers .= 'Date: ' . date("r") . "\r\n";
            $headers .= 'X-Mailer: ' . $siteName . "\r\n";
            
            // And now, mail it
            if (mail($to, $emailSubject, $emailMessage, $headers)){
                echo $thankyou;
            }
            else {
                echo $failure;
            }
        }
        // IF NO ERROR - END


    }

    // If web form has not been submitted, show a blank form
    else {
        showContactForm();
    }

    /*
     * If there have been errors, we want to return notification
     * We also want to be nice, and send any data already filled in back so they don't re-enter it
     */

    // If there are errors, and the contact E-mail is filled in
    if ($error != '' && isset($_POST["email"])){
        showContactForm($firstName, $lastName, $email, $telephone, $town, $validation, $message, $error);
    }

    // If there are errors, and the contact E-mail isn't filled in
    else if ($error != '' && !isset($_POST["email"])){
        showContactForm('', '', '', '', '', '', '', $error);
    }

    // Setup a function to display a contact form
    function showContactForm($firstName = "", $lastName = "", $email = "", $telephone = "", $town = "", $validation = "", $message = "", $error = '') { 
        
        if ($error != '') {
            echo '<div id="error" class="column">
                <div class="ui red message">
                    <div class="header">
                        Whoops, we seem to have a few errors!
                    </div>
                    <ul>' 
                    . $error . 
                    '</ul>
                </div>
            </div>';
        }

    ?>
        
        <form method="post" action="<?php $_SERVER["REQUEST_URI"] ?>" name="contactform" id="contactform">

        <input type="hidden" name="subject" id="subject" value="A contact message from the website">

        <div class="ui two fields">

            <div class="field">
                <label for="first-name">First Name:</label>
                <input type="text" name="first-name" id="first-name" placeholder="e.g John" value="<?php echo $firstName ?>" />
            </div>
            
            <div class="field">
                <label for="last-name">Last Name</label>
                <input type="text" name="last-name" id="last-name" placeholder="e.g Smith" value="<?php echo $lastName ?>" />
            </div>

        </div>

        <div class="ui two fields">
            
            <div class="field">
                <label for="email">Email:</label>
                <input type="email" name="email" id="email" placeholder="e.g John.Smith@email.com" value="<?php echo $email ?>" />
            </div>

            <div class="field">
                <label for="telephone">Telephone:</label>
                <input type="tel" name="telephone" id="telephone" placeholder="e.g 01234 567 890" value="<?php echo $telephone ?>" />
            </div>

        </div>

        <div class="ui two fields">
            
            <div class="field">
                <label for="town">Town:</label>
                <input type="text" name="town" id="town" placeholder="e.g Ipswich" value="<?php echo $town ?>" />
            </div>

            
            <div class="field">
                <label for="validation">Let's prove you're human, what's 2 + 2?</label>
                <input type="text" name="validation" id="validation" placeholder="2+2=?" value="<?php echo $validation ?>">
            </div>                                        

        </div>

        <div class="field">
            
            <div id="prefered-contact" class="inline fields">
                <div class="field">
                    <label>Prefered method of contact</label>
                  <div class="ui radio checkbox">
                    <input id="contact-telephone" name="contact-choice" checked="checked" value="telephone" type="radio">
                    <label for="contact-telephone">Telephone</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui radio checkbox">
                    <input id="contact-email" name="contact-choice" value="email" type="radio">
                    <label for="contact-email">Email</label>
                  </div>
                </div>
            </div>

        </div>

        <div class="field">
            <label for="message">Message:</label>
            <textarea name="message" rows="20" cols="20" id="message"><?php echo $message ?></textarea>
        </div>

        <input id="submit" type="submit" name="submit" value="Submit" class="ui submit button floated right" />
    </form>
   
   <?php }


?>