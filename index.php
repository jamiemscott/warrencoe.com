<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Warren Coe | Ipswich based fully qualified Approved Driving Instructor</title>
        <meta name="description" content="Warren Coe, a qualified Approved Driving Instructor working in Ipswich and surrounding areas.">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link href='//fonts.googleapis.com/css?family=Lato:400,300,700,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="./assets/css/styles.css">

        <link rel="icon" type="image/ico" href="favicon.ico">      

        <script src="./assets/js/vendor/modernizr-2.6.2.min.js"></script>
        
    </head>
    <body>
        <!--[if lt IE 9]>>
        <div class="ui page grid">

            <div class="warning twelve column">

                <div id="browser-warning" class="ui one column floating red warning message">
                    <i class="close icon"></i>
                    <div class="centered header">
                        You are using an <strong>outdated</strong> browser.
                    </div>
                        Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.
                </div>

            </div>

        </div>
        <![endif]-->
        
        <div class="ui page grid">

            <div class="twelve column">
                                
                <header class="ui one column grid">
                    <div class="row">
                        <div class="column">
                            <h1 class="ui right floated inverted header"><a href="/">Warren Coe School of Motoring</a></h1>
                        </div>
                    </div>
                </header>

                <nav id="navigation" class="ui three column grid">
                    <ul  class="ui fluid three item menu">
                        <li class="item"><a href="#about">About</a></li>
                        <li class="item"><a href="#pricing">Pricing</a></li>
                        <li class="item"><a href="#contact">Contact</a></li>
                    </ul>
                </nav>                

                <?php //BOF Promotional ?>
                <div id="promotional" class="ui three column middle center aligned grid">
                    <div class="equal height row">
                        <div class="column">
                            <p id="pass-rate-editable" class="editable">91% <em>Pass rate*</em> <span>*average over 2014</span></p>
                        </div>
                        <div class="column">
                            &nbsp;
                        </div>
                        <div class="column">
                            <p id="free-offer-editable" class="editable">Free <em>1st lesson*</em> <span>*see pricing</span></p>
                        </div>
                    </div>
                </div>
                <?php //EOF Promotional ?>
            
                <?php //BOF Contact Quote ?>
                <div id="contact-quote" class="ui center aligned one column grid">
                    <div class="row">
                        <p class="column">Get your self on the road, call today - 
                        <a id="land-line-editable" class="editable" href="tel:01473 680375">01473 680375</a> / <a id="mobile-editable" class="editable" href="tel:07740 203 332">07740 203 332</a></p>
                    </div>
                </div>
                <?php //EOF Contact Quote ?>

                <div id="main-content" class="row">

                    <?php //BOF Testimonials ?>
                    <div id="testimonials" class="ui four column stackable grid">
                        <div class="row">
                            <div id="testimonial-1-editable" class="editable testimonial column">
<p>"With his help I passed first time and there&rsquo;s no better recommendation than that" <em>Tom S, Ipswich</em></p>
</div>
                            <div id="testimonial-2-editable" class="editable column">
<p>"Warren had my best interest in mind at all times and with his help I managed to pass first time." <em>Sean Scannell</em></p>
</div>
                            <div id="testimonial-3-editable" class="editable column">
<p>"Great instructor from start to finish and I would recommend everyone to go with him. " <em>Ryan Buck</em></p>
</div>
                            <div id="testimonial-4-editable" class="editable column">
<p>"Relaxed. Incredibly professional. Warren really knows his stuff, he focases on the end result." <em>Daniel Harrison - Pinder</em></p>
</div>
                        </div>
                    </div>
                    <?php //EOF Testimonials ?>

                    <?php //BOF About ?>
                    <div id="about" class="ui one column grid">
                        <div class="column">
                            <h2 class="ui dividing header">About</h2>
                            <div class="ui two column stackable grid">
                                <div id="column-1-about-editable" class="editable column">
<p>Warren qualified as an approved driving instructor (ADI) in 2004. He has since gone on to specialize as a fleet trainer allowing him to assess company drivers. He also serves as a trainer and presenter for Suffolk County Council, delivering driver diversionary courses on behalf of Suffolk Police.</p>
<p>It is with Warren's wealth of knowledge and experience in these areas that enables his students to reach a much higher level of road safety, before they take their driving test.</p>
<p>As a passionate road safety professional, Warren not only teaches people to pass a test, but encourages safe driving for life.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
                                <div id="column-2-about-editable" class="editable column">
<p>This driving school prides itself on creating a very relaxed and safe environment in the car for all types of abilities, and will ensure you receive the support you need, whether it is theory, practical or moral support.</p>
<p>Your lessons will be structured around you and your individual needs, and keep you on track to ensure you make good progress all the way through your driving tuition.</p>
</div>
                            </div>
                        </div>
                    </div>
                    <?php //EOF About ?>

                    <?php //BOF Pricing ?>
                    <div id="pricing" class="ui one column grid">
                        <div class="column">
                            <h2 class="ui dividing header">Pricing</h2>
                            <div class="ui two column stackable grid">
                                <div id="column-1-pricing-editable" class="editable column">
<h3 class="ui header">Standard Bookings</h3>
<p>Single Hour &ndash; &pound;25.00 <br /> 1st Lesson &ndash; 2 hrs for the cost of an hour (&pound;25)</p>
<h3 class="ui header">Block Bookings</h3>
<p>Buy 5 hours &ndash; &pound;110 (&pound;15 discount) <br /> Buy 10 hours &ndash; &pound;220 (&pound;30 discount)</p>
</div>
                                <div id="column-2-pricing-editable" class="editable column">
<h3 class="ui header">Student Rates</h3>
<p>Call&nbsp;<a href="tel:01473 680375">01473 680375</a>&nbsp;/&nbsp;<a href="tel:07740 203 332">07740 203 332</a>&nbsp;to find out the lastest offers</p>
<h3 class="ui header">Promotions</h3>
<p>Call <a href="tel:01473 680375">01473 680375</a> / <a href="tel:07740 203 332">07740 203 332</a> to find out the lastest offers</p>
</div>
                            </div>
                        </div>
                    </div>
                    <?php //EOF Pricing ?>

                    <?php //BOF Contact ?>                    
                    <div id="contact" class="ui one column grid">
                        <div class="column">
                            <h2 id="contact-editable" class="editable ui dividing header">Contact</h2>
                            <p>To book a lesson or is you have an questions get in touch on <a href="tel:01473 680375">01473 680375</a> / <a href="tel:07740 203 332">07740 203 332</a> or send me a message using the form below.</p>
                        </div>
                        <div class="column">
                            <div class="ui form">

                                <?php include('assets/includes/contact-form.php'); ?>
                                
                            </div>
                        </div>

                    </div>
                    <?php //EOF Contact ?>

                    <?php //BOF Associations ?>
                    <div id="associations" class="ui center aligned six column stackable grid">
                        <div class="column association dsa">
                            <a href="//www.gov.uk/government/organisations/driving-standards-agency" title="Driving Standards Agency" target="_blank" class="dsa">DSA</a>
                        </div>                    
                        <div class="column association think">
                            <a href="//think.direct.gov.uk/" title="Think Road Safety" target="_blank">Think Road Safety</a>
                        </div>                    
                        <div class="column association dsa-approved">
                            <a href="//www.gov.uk/government/organisations/driving-standards-agency" title="Drivng Standards Agency Approved Driving Instructor" target="_blank">Drivng Standards Agency Approved Driving Instructor</a>
                        </div>                    
                        <div class="column association pass-plus">
                            <a href="//www.gov.uk/pass-plus" title="Pass Plus" target="_blank">Pass Plus</a>
                        </div>                    
                        <div class="column association dia">
                            <a href="//www.driving.org/" title="Driving Instructors Association" target="_blank">Driving Instructors Association</a>
                        </div>
                        <div class="column association sitelock">
                            <a href="//www.sitelock.com/verify.php?site=www.warrencoe.com" target="_blank">SiteLock</a>
                        </div>
                    </div>
                    <?php //EOF Associations ?>

                </div>

                <footer class="ui two column grid">
                    <div class="row">
                        <div class="column">
                            <p>&copy; <?php echo date('Y'); ?> Warren Coe. All Rights Reserved</p>
                        </div>
                        <div class="right aligned column">
                            <p id="address-editable" class="editable">2 Yewtree Rise, Ipswich, IP8 3R</p>
                        </div>
                    </div>
                </footer>

            </div>

        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="./assets/js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

        <!--[if lt IE 7]>
        <script src="./assets/js/vendor/respond.js"></script>
        <![endif]-->

        <script src="./assets/js/vendor/jquery.cookie.min.js"></script>        
        <script src="./assets/js/vendor/jquery.cookiecuttr.js"></script>
        <script src="./assets/js/vendor/semantic.min.js"></script>
        <script src="./assets/js/vendor/selectivizr-min.js"></script>        
        <script src="./assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-54672011-1', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>
